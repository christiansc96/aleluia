// $(document).ready(function(){
    function next(){        
        $("#form-post-login").fadeOut('fast','linear',function(){
            $("#form-post-register").fadeIn('fast','linear');
        });
    }

    function back(){
        $("#form-post-register").fadeOut('fast','linear',function(){
            $("#form-post-login").fadeIn('fast','linear');
        });
    }

    $( "#form-post-register" ).validate({
        errorElement: 'div',
        rules: {
            fullname: "required",
            email: {
                required: true,
                email: true,
            },
            password: "required",
            password_confirmation: {
                equalTo: "#password",
                required: true,
            }
        },
        messages: {
            fullname:{
                required: "Este es obligatorio.",
            },
            email:{
                required: "El campo email es obligatorio.",
                email: "El email debe ser correcto.",
            },
            password:{
                required: "El campo contraseña es obligatorio."
            },
            password_confirmation:{
                required: "Este campo es obligatorio.",
                equalTo: "Las contraseñas no coinciden.",
            }

        },
        submitHandler: function(form) {
            $.ajax({
                type:     "post",
                data:     $(form).serialize(),
                cache:    false,
                url:      "signup",
                dataType: "json",
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var responseText = jQuery.parseJSON(xhr.responseText);
                    
                    // Erro de email si no es unico
                    if(responseText.email !== null){
                        $("#email_signup span").html(responseText.email).fadeIn();
                        // $("#email_signup label").css('color', '#b30000');
                        // $("#email_signup input").css('border-color', '#b30000');
                    }
                    
                },
                success: function (response) {
                    $("#alert").text(response).fadeIn('fast', 'linear');
                    $("#form-post-register").fadeOut('fast','linear',function(){
                        $("#form-post-login").fadeIn('fast','linear');
                    });
                    $("#alert").text(response).fadeOut(7000, 'swing');
                }
            });
        }
    });

    function register(){
        
    }

// });