<?php
// Auth::routes();

Route::get('/', 'FrontController@index')->name('fe.home');

Route::get('Autocomplete', 'FrontController@autocomplete');

Route::get('Autocomplete2', 'FrontController@autocomplete_products_page');

Route::get('productos', 'FrontController@products')->name('fe.products');

Route::get('productos/{filter}', 'FrontController@filter')->name('fe.products.filter');

// Route::get('productos/{uses}', 'FrontController@uses_filter')->name('fe.products.uses_filter');

// Route::get('productos/{colors}', 'FrontController@colors_filter')->name('fe.products.colors_filter');

// Route::get('productos/{sizes}', 'FrontController@sizes_filter')->name('fe.products.sizes_filter');

Route::get('producto/{product}', 'FrontController@show_product')->name('fe.show.products');

Route::get('directorio', 'FrontController@directory')->name('fe.directory');

Route::get('directorio/{state}', 'FrontController@show_directory')->name('fe.show.directory');

Route::get('ingresar', 'ClientLogin@showLoginForm')->name('fe.showLoginForm');

Route::post('sigup', 'ClientRegister@register')->name('fe.register');

Route::post('signin', 'ClientLogin@login')->name('fe.login');

Route::post('cerrar-sesion', 'ClientLogin@logout')->name('fe.logout');

Route::middleware(['role:cliente'])->group(function () {

	Route::resource('carrito','CartController');

	Route::post('carrito/solicitud-pedido', 'CartController@storeOrder')->name('storeOrder');

	Route::get('cart/datatable', 'CartController@datatable')->name('datatable.carrito');

	Route::resource('mis-pedidos','MyOrdersController');

});

