<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color = new Color();
        $color->name = 'negro';
        $color->active = 1;
        $color->save();

        $color = new Color();
        $color->name = 'blanco';
        $color->active = 1;
        $color->save();

        $color = new Color();
        $color->name = 'azul';
        $color->active = 1;
        $color->save();

        $color = new Color();
        $color->name = 'verde';
        $color->active = 1;
        $color->save();

        $color = new Color();
        $color->name = 'rojo';
        $color->active = 1;
        $color->save();

        $color = new Color();
        $color->name = 'gris';
        $color->active = 1;
        $color->save();
    }
}
