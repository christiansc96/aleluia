<?php

use Illuminate\Database\Seeder;
use App\Manufacturer;

class ManufacturerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manufacturer = new Manufacturer();

        $manufacturer->name = 'España';

        $manufacturer->active = 1;

        $manufacturer->save();

        $manufacturer = new Manufacturer();

        $manufacturer->name = 'Portugal';

        $manufacturer->active = 1;

        $manufacturer->save();

        $manufacturer = new Manufacturer();

        $manufacturer->name = 'China';

        $manufacturer->active = 1;

        $manufacturer->save();

        $manufacturer = new Manufacturer();

        $manufacturer->name = 'Colombia';

        $manufacturer->active = 1;

        $manufacturer->save();
    }
}
