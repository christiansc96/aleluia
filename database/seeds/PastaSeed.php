<?php

use Illuminate\Database\Seeder;
use App\Pasta;

class PastaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pasta = new Pasta();
        $pasta->name = 'pasta roja';
        $pasta->active = 1;
        $pasta->save();

        $pasta = new Pasta();
        $pasta->name = 'porcelanato';
        $pasta->active = 1;
        $pasta->save();

        $pasta = new Pasta();
        $pasta->name = 'porcelanico gres';
        $pasta->active = 1;
        $pasta->save();
    }
}
