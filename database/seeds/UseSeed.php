<?php

use Illuminate\Database\Seeder;
use App\Uso;

class UseSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $use = new Uso();
        $use->name = 'paredes';
        $use->active = 1;
        $use->save();

		$use = new Uso();
        $use->name = 'pisos';
        $use->active = 1;
        $use->save();        
    }
}
