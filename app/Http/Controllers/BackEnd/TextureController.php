<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Texture;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class TextureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.textures.textures');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $texture = new Texture();

        $texture->name = trim($request->name);

        $texture->active = 1;

        $texture->save();

        return 'Textura creado!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $texture = Texture::findOrFail($id);

        return $texture;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $texture = Texture::findOrFail($id);

        $texture->name = trim($request->name);

        $texture->active = 1;

        $texture->save();

        return 'Textura actualizado!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $texture = Texture::findOrFail($id);

        $message = 'Textura '. $texture->name .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $texture->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $texture = Texture::findOrFail($id);

        if($texture->active == 1):

            $texture->active = 0;

            $texture->save();

            $textstatus = 'Deshabilitado';

        else:

            $texture->active = 1;

            $texture->save();

            $textstatus = 'Habilitado';

        endif;

        return 'Textura '.$texture->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $textures = Texture::all();

        return $datatables->of($textures)
            ->addColumn('estatus', function($texture){
                if($texture->active == 1):

                    return '<span class="label label-sm label-success"> Habilitado </span>';

                endif;

                return '<span class="label label-sm label-warning"> Deshabilitado </span>';
            })
            ->addColumn('opciones', function($texture){
                if($texture->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Opciones
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$texture->id.')">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$texture->id.')">
                                                <i class="fa fa-times"></i> Deshabilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$texture->id.')">
                                                <i class="fa fa-trash"></i> Remover 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Opciones
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$texture->id.')">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$texture->id.')">
                                                <i class="fa fa-check"></i> Habilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$texture->id.')">
                                                <i class="fa fa-trash"></i> Remover 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['estatus', 'opciones' ])->make(true);
    }
}
