<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\User;
use App\Product;
use App\Order;
use Session;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = Cart::instance('tuceramica')->content();

        // if(auth()->user()->roles[0]->id == 2):

        //     $user = User::find(auth()->user()->id);
            
        //     $asociados = $user->vendedores;

        // endif;

        return view('frontend.carrito', compact('cartItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::findOrFail($request->id);

        Cart::instance('tuceramica')->add($product->code, $product->name, $request->qty, $product->price->price, ['coin' => $product->price->coin, 'image' => $product->image[0]->name, 'product_id' => $product->id]);

        return 'Producto enviado al carrito de compras!';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Cart::instance('tuceramica')->get($id);

        if($request->up_down == 1)
        {
            $cart->qty++;
        }else{
            $cart->qty--;            
        }

        return Cart::instance('tuceramica')->content();
        // return 'Cantidad actualizada!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('tuceramica')->remove($id);

        return 'Producto eliminado del carrito!';
    }

    public function datatable(Datatables $datatables)
    {

        $cartItems = Cart::instance('tuceramica')->content();

        return $datatables->of($cartItems)
            ->addColumn('code', function($cartItem){
               return $cartItem->id;
            })
            ->addColumn('image', function($cartItem){
               return '<img class="shopping-product" src="'.asset('storage/images/products/'.$cartItem->options->product_id.'/'.$cartItem->options->image).'" style="max-width: 100px;" alt="'.$cartItem->options->image.'">';
            })
            ->addColumn('name', function($cartItem){
                return $cartItem->name;
            })
            ->addColumn('price', function($cartItem){
                return $cartItem->price.$cartItem->options->coin;
            })
            ->addColumn('qty', function($cartItem){
                return '<form method="POST" action="'.route("carrito.update", ":CART_ID").'" id="form-update-'.$cartItem->rowId.'">
                            <div class="input-group quantity-number">
                                '.csrf_field().'
                                '.method_field('PATCH').'
                                <input type="text" class="form-control" value="'.$cartItem->qty.'" id="qty" name="qty">
                                <div class="input-group-btn-vertical">
                                    <div class="btn" onclick="updateData(\'' .$cartItem->rowId. '\')"><i class="fa fa-angle-up"></i>
                                    </div>
                                    <div class="btn" onclick="updateData(\'' .$cartItem->rowId. '\')"><i class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </div>
                        </form>';
            })
            ->addColumn('total', function($cartItem){
                return $cartItem->price * $cartItem->qty;
            })
            ->addColumn('acciones', function($cartItem){
                $cadena2 = '<a class="btn-close" href="#" onclick="deleteData(\'' .$cartItem->rowId. '\')"><i class="fa fa-close"></i></a>';
                return $cadena2;
            })->rawColumns(['qty', 'actions', 'image'])->make(true);
    }
}
