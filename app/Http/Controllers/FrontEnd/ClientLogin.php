<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class ClientLogin extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontend.register-login');
    }

    protected function guard()
	{
	    return Auth::guard('clients');
	}

	public function authenticated(Request $request, $user)
    {
        if($user->active != 1)
        {
            $this->guard()->logout();
            
            return redirect('/ingresar')->with('status', 'Tú usuario no está aprobado por el sistema, intenta luego o comunicate con nuestro soporte.');
        }
        return redirect('/');
    }
}
