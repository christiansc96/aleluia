<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasta extends Model
{
	public function product()
    {
    	return $this->hasMany('App\Product', 'pasta_id');
    }

    protected $table ='pastas';

    protected $guarded = [];
}
