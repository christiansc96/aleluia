<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    
	public function product()
	{
		return $this->belongsToMany('App\Product', 'color_product')->withTimestamps();
	}

    protected $table ='colors';

    protected $guarded = [];

}
