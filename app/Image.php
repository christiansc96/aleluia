<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	public function product()
	{
		return $this->belongsToMany('App\Product', 'image_product')->withTimestamps();
	}

	public function user()
	{
		return $this->hasOne('App\User', 'user_id');
	}

	public function store()
	{
		return $this->hasOne('App\Store', 'image_id');
	}

	public function showcase()
	{
		return $this->hasOne('App\ShowCase', 'image_id');
	}

    protected $table ='images';

    protected $guarded = [];
}
