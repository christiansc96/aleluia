<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    public function product()
    {
    	return $this->hasMany('App\Product', 'manufacturer_id');
    }

    protected $table ='manufacturers';

    protected $guarded = [];
}
