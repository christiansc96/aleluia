<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }

    public function client()
    {
        return $this
            ->hasMany('App\Client', 'role_id');
    }
    
    protected $table = 'roles';
    protected $guarded = [];
}
