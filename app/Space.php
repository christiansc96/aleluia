<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    public function product()
    {
    	return $this->hasMany('App\Product', 'space_id');
    }

    protected $table = 'spaces';
    
    protected $guarded = [];
}
