<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130786499-3"></script>
    {{-- <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-130786499-3');
    </script> --}}
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta name="description" content="Tu Cerámica">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

    <!-- Page Title -->
    <title>{{ config('app.name') }} | @yield('title')</title>

    <!-- Favicon -->
    <link rel="icon" href="img/favicon.png" type="image/png" />

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('css/media.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('css/flexslider-set.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

    @yield('styles')

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Loader Begin -->

    <div class="loader">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>

    <!-- Loader End -->

    <!-- Back To Top Begin -->

    <div id="back-top">
        <a href="#" class="scroll">
            <i class="arrow_carrot-up"></i>
        </a>
    </div>

    <!-- Back To Top End -->

    <!-- Site Wrapper Begin -->

    <div class="wrapper">

        <!-- Header Begin -->

        <header>
            <nav class="main-nav menu-dark menu-transparent nav-transparent">
                <div class="col-md-12">
                    <div class="navbar">
                        <div class="brand-logo">
                            <a href="{{ route('fe.home') }}" class="navbar-brand">
                                <img src="{{asset('img/logo_aleluia.png')}}" alt="Aleluia" />
                            </a>
                        </div>
                        <!-- brand-logo -->

                        <div class="navbar-header">
                            <div class="inner-nav right-nav">
                                <ul class="rightnav-links">
                                    <li>
                                        <a href="#" id="search-trigger"><i class="fa fa-search"></i></a>
                                        <form action="#" id="search" method="get" class="">
                                            <div class="input">
                                                <div class="container">
                                                    <input class="search" placeholder="Consiga el producto que busca..." type="text" id="buscar_producto">
                                                    <input type="hidden" name="producto_idproducto" id="producto_idproducto">
                                                    <div id="autocomplete_producto_container"></div>
                                                    <button class="submit" type="submit" value="close"><i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                                <!-- /container -->
                                            </div>
                                            <!-- /input -->
                                            <button class="icon_close" id="close" type="reset"></button>
                                        </form>
                                        <!-- /form -->
                                    </li>
                                    
                                    @if (!Auth::guest() || Auth::guard('clients')->check())
                                        <li>
                                            <a href="javascript:;" class="side-cart-toggle">
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="notice-num" ><p id="count">{{Cart::instance('tuceramica')->count()}}</p></span>
                                            </a>
                                        </li>
                                    @endif
                                        
                                    <li>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapse">
                                            <span class="sr-only"></span>
                                            <i class="fa fa-bars"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <!-- /right-nav -->
                        </div>
                        <!-- navbar-header -->

                        <div class="custom-collapse navbar-collapse collapse inner-nav margin-left-100 pull-right">
                            <ul class="nav navbar-nav nav-links">
                                <li class="dropdown classic-dropdown"><a href="http://tuceramica.com/" class="dropdown-toggle">TUCERÁMICA</a></li>
                                {{-- <li class="dropdown classic-dropdown"><a href="{{route('fe.home')}}" class="dropdown-toggle">Home</a></li> --}}
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Quiénes somos</a></li>
                                <!-- /dropdown -->

                                <li class="dropdown classic-dropdown"><a href="{{ route('fe.products') }}" class="dropdown-toggle" {{-- data-toggle="dropdown" --}}>Productos {{-- <i class="fa fa-angle-down"></i> --}}</a>
                                    {{-- <ul class="dropdown-menu">
                                        <li class="submenu dropdown">
                                            <a href="productos.html" class="dropdown-toggle" data-toggle="dropdown">Categoria uno</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="productos.html">Sub-categoria uno</a>
                                                </li>
                                                <li><a href="productos.html">Sub-categoria dos</a>
                                                </li>
                                            </ul>
                                            <!-- /dropdown-menu -->
                                        </li>
                                        <!-- /submenu -->
                                        <li><a href="{{ route('fe.products') }}">Categoria dos</a>
                                        </li>
                                        <li><a href="productos.html">Categoria tres</a>
                                        </li>
                                    </ul> --}}
                                    <!-- /dropdown-menu -->
                                </li>
                                <!-- /dropdown -->

                                {{-- <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a></li> --}}
                                <!-- /dropdown -->

                                {{-- <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Contacto</a></li> --}}

                                <li class="dropdown classic-dropdown"><a href="{{ route('fe.directory') }}" class="dropdown-toggle">Directorio</a></li>
                                @if(Auth::guest())
                                    @if (Auth::guard('clients')->check())

                                        <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></li>
                                        <form id="logout-form" action="{{ route('fe.logout') }}" method="POST" style="display: none;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </form>
                                    @else
                                        <li class="dropdown classic-dropdown"><a href="{{ route('fe.showLoginForm') }}" class="dropdown-toggle">Ingresar</a></li>
                                    @endif
                                @else
                                    <li class="dropdown classic-dropdown"><a href="#" class="dropdown-toggle" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                @endif
                                <!-- /dropdown -->
                                <!-- /dropdown -->
                            </ul>
                            <!-- /nav -->
                        </div>
                        <!-- /collapse -->
                    </div>
                    <!-- /navbar -->
                    
                    @include('frontend.cart')

                </div>
                <!-- /container -->
            </nav>
            <!-- /nav -->
        </header>

        <!-- Header End -->

        @yield('content')

        <!-- Footer Begin -->

        <footer class="footer-2">
            <div class="container">
                <div class="row">
                    <div class="footer-social text-center">
                        <ul>
                            <li><a href="#">Facebook</a>
                            </li>
                            <li><a href="#">Twitter</a>
                            </li>
                            <li><a href="#">Instagram</a>
                            </li>
                            <li><a href="#" class="bd-right">YouTube</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-social -->

                    <div class="col-md-6 col-sm-6 about-widget padding-top-50" style="margin-top: 2px;">
                    <img src="{{asset('img/logo.png')}}" alt="" />
                        <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, antepo suerit litterarum formas humanitatis per seacula quarta. Typi non habent claritatem insitam est usus legent is iis qui facit claritatem. Investigatione littera gothica, quam nunc putamus parum claram, antepo suerit litterarum formas humanitatis per seacula quarta.</p>
                        <ul>
                            <li><img src="{{asset('img/icon-pointer.png')}}" alt="" />Caracas - Venezuela</li>
                            <li><img src="{{asset('img/icon-phone.png')}}" alt="" />+58 426.9139991</li>
                            <li><img src="{{asset('img/icon-phone.png')}}" alt="" />+58 212 7413687</li>
                            <li><img src="{{asset('img/icon-mail.png')}}" alt="" />tuceramicaweb@gmail.com</li>
                        </ul>
                    </div>
                    <!-- /column -->

                    <div class="col-md-3 col-sm-6 twitter-feed padding-top-40">
                        <h3>twitter</h3>
                        <ul class="margin-top-60">
                            <li>Typi non habent claritatem insitam est usus legent is iis qui facit claritatem. Investigatione <a href="http://arquetitek.com" target="blank">http://arquetitek.com</a>
                                <span><i class="social_twitter"></i>12 days ago</span>
                            </li>
                            <li>Typi non habent claritatem insitam est usus legent is <a href="http://arquetitek.com" target="blank">http://arquetitek.com</a>
                                <span><i class="social_twitter"></i>10 days ago</span>
                            </li>
                        </ul>
                    </div>
                    <!-- /column -->

                    <div class="col-md-3 col-sm-6 news padding-top-40">
                        <h3>Blog</h3>
                        <ul class="margin-top-65">
                            <li>
                                <div class="news-image">
                                    <img src="{{asset('img/blog/blog1.jpg')}}" style="max-width: 75px;" alt="" />
                                </div>
                                <!-- /news-image -->
                                <div class="news-details">
                                    <span class="title"><a href="#">titulo de la publicacion</a></span>
                                    <span class="date">mayo 11, 2018</span>
                                </div>
                                <!-- /news-details -->
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="news-image">
                                    <img src="{{asset('img/blog/blog2.jpg')}}" style="max-width: 75px;" alt="" />
                                </div>
                                <!-- /news-image -->
                                <div class="news-details">
                                    <span class="title"><a href="#">titulo de la publicacion</a></span>
                                    <span class="date">mayo 11, 2018</span>
                                </div>
                                <!-- /news-details -->
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                        <!-- /ul -->
                    </div>
                    <!-- /column -->

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->

            <div class="footer-rights">
                <div class="container">
                    <div class="copyright-info text-center">
                        <p class="ft-desc">Copyright 2018 Tuceramica all rights reserved. Desarrollado por<a href="http://arquetitek.com" target="blank"> Arquetitek</a>
                        </p>
                    </div>
                    <!-- /copyright-info -->
                    <div class="footer-payments pull-right">
                        <ul>
                            <li><img src="{{asset('img/visa.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/mastercard.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/discover.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/americanexpress.png')}}" alt="" />
                            </li>
                            <li><img src="{{asset('img/paypal.png')}}" alt="" />
                            </li>
                        </ul>
                    </div>
                    <!-- /footer-payments -->
                </div>
                <!-- /container -->
            </div>
            <!-- /footer-rights -->
        </footer>

        <!-- Footer End -->


    </div>

    <!-- Site Wrapper End -->

    <!--- Scripts -->

    <script src="{{asset('js/lib/jquery.min.js')}}"></script>
    <script src="{{asset('js/vendors/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('js/lib/moderniz.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/vendors/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.bxslider.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('js/vendors/jquery-ui.js')}}"></script>
    <script src="{{asset('js/vendors/flexslider-init.js')}}"></script>
    <script src="{{asset('js/vendors/smoothscroll.js')}}"></script>

    @yield('scripts')

    <script type="text/javascript">
      $(document).ready(function(){

        $("#buscar_producto").keyup(function(){
          $.ajax({
              url: '/Autocomplete',
              type:'GET',
              dataType:'json',
              data:{info:$('#buscar_producto').val()}
          }).done(function(data){
            console.log(data);
            if( data != 0){
              $("#producto_idproducto").val(data.id);
              $("#autocomplete_producto_container").fadeIn();
              $("#autocomplete_producto_container").html(data.output);
              $(document).on('click','#opcion_productos', function(){
                // $("#buscar").prop("href", "http://peltca.pro/Productos/"+data.id);
                $("#buscar_producto").val($(this).text());
                $("#autocomplete_producto_container").fadeOut();
              });
            }else{
              $("#buscar").prop("href", "#");
              $("#autocomplete_producto_container").fadeOut();
            }
          });
        });

      });
    </script>


    <script type="text/javascript">
        $(window).on('load', function() {
            $('#newsletterModal').modal('show');
        });

    </script>
</body>
</html>