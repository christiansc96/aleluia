@extends('layouts.back')

@section('title', 'admin descripción de productos')

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
{{-- dataTables --}}
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Producto - {{ $product->name }}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <a href="{{ route('admin-productos.index') }}">
                            Productos
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            {{ $product->name }}
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="products">
					
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#description" data-toggle="tab">
								Descripción
							</a>
						</li>
						<li>
							<a href="#galery" data-toggle="tab">
								Galeria de imagenes
							</a>
						</li>	
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="description">
						    <div class="row">
						      	<div class="col-md-12">
								    @include('backend.products.description.detail
								    ')     
						      	</div>
						    </div>
						</div>		
						<div class="tab-pane fade" id="galery"> 
						    <div class="row">
						      	<div class="col-md-12">
								    @include('backend.products.description.galery
								    ') 
						      	</div>
						    </div>
						</div>						
					</div> 

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
{!!Form::open(['route'=>[ 'admin-galeria.destroy', ':IMAGE_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-galeria.update', ':IMAGE_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
	var galery = new Vue({
        el: '#galery',
        data: {
            save_method: null,
            method: null,
        },
        methods: {
            addForm: function(){
                galery.save_method = 'add';
                galery.method = 'POST';
                $('#modal-form').modal('show');
                document.getElementById("files").value = "";
                $('.modal-title').text('Agregar imagen');
            },
            modal_form: function(product_id){
                this.$validator.validateAll().then((result) => {
                    if(result){

                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });
                        
                        let formData = new FormData();

                        if (galery.save_method == 'add'){

                            var form = $('#form-post');

                            var url = form.attr('action');

                        }

                        formData.append('_method', galery.method);
                        formData.append('product_id', product_id);
                        formData.append('image', document.getElementById('image').files[0]);
                        
                        axios.post(url, formData,{
                            headers: {
                                'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                            }
                        }).then(function (response) {
                            console.log(response);
                            dialog.modal('hide');
                            table.ajax.reload();
                            swal({
                                title: 'Aprobado!',
                                text: response.data,
                                type: 'success',
                                timer: '1500'
                            });
                            $('#modal-form').modal('hide');
                        }).catch(function (error) {
                            console.log(error);
                            dialog.modal('hide');
                            var refrescar = bootbox.dialog({
                                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                                closeButton: false,
                                buttons: {
                                    refrescar: {
                                        label: '<i class="fa fa-refresh"></i> Refrescar',
                                        callback: function (result) {
                                            location.reload(true);
                                        }
                                    }
                                }
                            });
                        });
                        return;
                    }
                    bootbox.alert({
                        message: "<p class='text-center'>Debe corregir los errores</p>",
                        backdrop: true,
                        closeButton: false,
                    });
                });
            }
        }
    });

    var table = $('#galery-table').DataTable({
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "_MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ de _TOTAL_ entradas",
                    "sInfoEmpty":      "Mostrando de 0 a 0 de 0 entradas",
                    "sInfoFiltered":   "(filtrado de _MAX_ entradas totales)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                processing: true,
                serverSide: true,
                ajax: "{{ route('superadmin.datatable.galery.product', $product->id) }}",
                columns: [
                    {data: 'image', name: 'image'},
                    {data: 'destacado', name: 'destacado'},
                    {data: 'opciones', name: 'opciones', orderable: false, searchable: false}
                ]
            });

    function outstandingData(id, product_id)
    {
        var reactive = bootbox.dialog({
            message: "<p class='text-center'>Estas seguro?</p>",
            closeButton: false,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success',
                    callback: function () {
                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });
                        let formData = new FormData();
                        var form = $('#form-update');
                        var url = form.attr('action').replace(':IMAGE_ID', id);
                        formData.append('_method', 'PATCH');
                        formData.append('product_id', product_id);
                        axios.post(url, formData,{
                            headers: {
                                'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                            }
                        })
                        .then(function (response) {
                            dialog.modal('hide');
                            console.log(response.data);
                            table.ajax.reload();
                            swal({
                                title: 'Updated status!',
                                text: response.data,
                                type: 'success',
                                timer: '1500'
                            });
                        }).catch(function (error) {
                            dialog.modal('hide');
                            console.log(error);
                            var refrescar = bootbox.dialog({
                                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                                closeButton: false,
                                buttons: {
                                    refrescar: {
                                        label: '<i class="fa fa-refresh"></i> Refrescar',
                                        callback: function (result) {
                                            location.reload(true);
                                        }
                                    }
                                }
                            });
                        });
                    }
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger',
                }
            },
        });
    }
    
    function removeData(id)
    {
        var form = $('#form-delete');
        var url = form.attr('action').replace(':IMAGE_ID', id);
        var data = form.serialize();

        var eliminar = bootbox.dialog({
            message: "<p class='text-center'>Quiere eliminar esta <strong>imagen</strong>?</p>",
            closeButton: false,
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success',
                    callback: function () {
                        var dialog = bootbox.dialog({
                            message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                            closeButton: false
                        });
                        axios.delete(url, data)
                        .then(function (response) {
                            dialog.modal('hide');
                            table.ajax.reload();
                            if(response.data == 'Este producto no puede tener más de 4 imagenes.'){
                                swal({
                                    title: 'Error',
                                    text: response.data,
                                    type: 'warning',
                                    timer: '1500'
                                });
                            }else{
                                swal({
                                    title: 'Removido!',
                                    text: response.data,
                                    type: 'success',
                                    timer: '1500'
                                });
                            }
                        }).catch(function (error) {
                            dialog.modal('hide');
                            console.log(error);
                            var refrescar = bootbox.dialog({
                                title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                                message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                                closeButton: false,
                                buttons: {
                                    refrescar: {
                                        label: '<i class="fa fa-refresh"></i> Refrescar',
                                        callback: function (result) {
                                            location.reload(true);
                                        }
                                    }
                                }
                            });
                        });
                    }
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger',
                }
            },
        });
    }
</script>
@endsection