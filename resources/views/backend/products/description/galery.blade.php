<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="alert alert-info text-center" role="alert">
            Está galeria sólo debe tener un total de 4 fotos. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h4>Galeria de imagenes</h4>
        <div class="portlet box dark">
            <div class="portlet-title">
                <div class="caption">
                    Ultimas imagenes agregadas
                </div>
                <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
                    Agregar imagen
                </a>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="galery-table">
                    <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
                        <tr>
                            <th>imagen</th>
                            <th>destacado</th>
                            <th>opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @include('backend.products.description.form-galery')
</div>